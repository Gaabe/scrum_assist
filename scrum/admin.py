from django.contrib import admin
from .models import Projeto

class ProjetoAdmin(admin.ModelAdmin):
	list_display = ('nome', 'data_inicio', 'data_fim')

admin.site.register(Projeto, ProjetoAdmin)