from django.db import models

class Projeto(models.Model):
	class Meta:
		verbose_name = "Projeto"
		verbose_name_plural = "Projetos"

	nome = models.CharField(max_length=200)
	data_inicio = models.DateField()
	data_fim = models.DateField()
