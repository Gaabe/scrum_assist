from django.contrib import admin
from .models import Administrador, Membro

class UsuarioAdmin(admin.ModelAdmin):
	readonly_fields = ['show_edit_password_url', ]

	fieldsets = (
		((u'Informações Pessoais'), {
			'fields': ('username',
					   'show_edit_password_url',
					   'first_name',
					   'last_name',
					   'email',
					   'foto')
		}
		),
	)

	add_fieldsets = (
		((u'Informações Pessoais'), {
			'fields': ('username',
					   'password',
					   'first_name',
					   'last_name',
					   'email',
					   'foto')
		}
		),
	)

	def get_fieldsets(self, request, obj=None):
		if not obj:
			return self.add_fieldsets
		return super(UsuarioAdmin, self).get_fieldsets(request, obj)

	def show_edit_password_url(self, obj):
		from django.utils.html import format_html
		admin_password_url = "/admin/{app_label}/{model_name}/{id}/password".format(app_label=obj._meta.app_label, model_name=obj._meta.model_name,id=obj.id)
		return format_html("*****  (<a href='{url}'>{change_text}</a>)", url=admin_password_url, change_text='Alterar Senha')
	show_edit_password_url.short_description = "Senha"

	def save_model(self, request, obj, form, change):
		if not obj.id:
			obj.set_password(obj.password)
		super(UsuarioAdmin, self).save_model(request, obj, form, change)

class AdministradorAdmin(UsuarioAdmin):
	list_display = ('username', 'first_name', 'last_name')

class MembroAdmin(UsuarioAdmin):
	list_display = ('username', 'first_name', 'last_name', 'cpf')

admin.site.register(Administrador, AdministradorAdmin)
admin.site.register(Membro, MembroAdmin)
