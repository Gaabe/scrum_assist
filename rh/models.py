from django.db import models
from django.contrib.auth.models import AbstractUser

class Usuario(AbstractUser):

	cpf = models.CharField(max_length=14)
	foto = models.ImageField(
		null=True,
		blank=True,
		upload_to='uploads/usuario/foto/',
	)

	def save(self, *args, **kwargs):
		self.is_staff = True
		super(Usuario, self).save()

class Administrador(Usuario):
	class Meta:
		verbose_name = 'Administrador'
		verbose_name_plural = 'Administradores'

class Membro(Usuario):
	class Meta:
		verbose_name = 'Membro'
		verbose_name_plural = 'Membros'